<?php
return [
    'labels' => [
        'WxShopClassify' => 'SHOP分类',
        'wx-shop-classify' => 'WxShopClassify',
    ],
    'fields' => [
        'name' => '分类名称',
        'intro' => '分类介绍',
        'pic' => '分类图',
        'bg_img' => '背景图',
        'parent_id' => '父级id',
        'sort' => '排序',
        'state' => '状态',
    ],
    'options' => [
    ],
];
